@extends('layouts.app')

@section('content')
<div class="container">

	<div class="row">
    	<div class="col-lg-12 margin-tb">
	        <div class="pull-left">
	            <h2>User Management</h2>
	        </div>
	        <div class="pull-right">
	            <a class="btn btn-success" href="#"> Create New User </a>
	        </div>
	    </div>
		    <div class="alert alert-success">
				<p>提示信息！</p>
			</div>
	    <table class="table table-bordered">
			<tbody>
				<tr>
					<th>No</th>
					<th>Name</th>
					<th>Email</th>
					<th>Roles</th>
					<th width="280px">Action</th>
				</tr>

				<tr>
					<td>1</td>
					<td>admin</td>
					<td>admin@admin.com</td>
					<td>
		                <label class="label label-success">superadmin</label>
					</td>
					<td>
						<a class="btn btn-info" href="#">Show</a>
						<a class="btn btn-primary" href="#">Edit</a>
					</td>
				</tr>

			</tbody>
		</table>
		<!--<ul class="pagination">
			<li class="disabled"><span>«</span></li> 
			<li class="active"><span>1</span></li>
			<li><a href="/home/users?page=2">2</a></li>
			<li><a href="/home/users?page=3">3</a></li>
			<li><a href="/home/users?page=4">4</a></li>
			<li><a href="/home/users?page=5">5</a></li>
			<li><a href="/home/users?page=6">6</a></li>
			<li><a href="/home/users?page=7">7</a></li>
			<li><a href="/home/users?page=8">8</a></li>
			<li class="disabled"><span>...</span></li>
			<li><a href="/home/users?page=1011">1011</a></li>
			<li><a href="/home/users?page=1012">1012</a></li> 
			<li><a href="/home/users?page=2" rel="next">»</a></li>
		</ul>-->
	</div>
</div>
@endsection
