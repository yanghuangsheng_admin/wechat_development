@extends('layouts.app')

@section('content')
<div class="container">
            
	<div class="row">
	    <div class="col-lg-12 margin-tb">
	        <div class="pull-left">
	            <h2>Create New Role</h2>
	        </div>
	        <div class="pull-right">
	            <a class="btn btn-primary" href="{{ route('roles') }}"> Back</a>
	        </div>
	    </div>
	</div>

	
	<form method="POST" action="{{ route('roles.update', ['id' => $role->id]) }}" accept-charset="UTF-8">
        <input type="hidden" name="_method" value="PATCH">
        {{ csrf_field() }}
    	<div class="row">

    		<div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input placeholder="Name" class="form-control" name="name" type="text" value="{{ $role->name }}">
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Display Name:</strong>
                    <input placeholder="Display Name" class="form-control" name="display_name" type="text" value="{{ $role->display_name }}">
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Description:</strong>
                    <textarea name="description" id="" class="form-control" cols="30" rows="10">{{ $role->description }}</textarea>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Permission:</strong><br>
                    @foreach ($permissions as $permission)
                    <label for=""><input type="checkbox" name="permissions[]" value="{{ $permission->id }}"> {{ $permission->display_name }}</label><br>
                    @endforeach
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
    			<button type="submit" class="btn btn-primary">Submit</button>
            </div>

    	</div>
	</form>

</div>
@endsection