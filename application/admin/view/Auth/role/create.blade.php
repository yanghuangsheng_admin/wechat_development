@extends('layouts.app')

@section('content')
<div class="container">
            
	<div class="row">
	    <div class="col-lg-12 margin-tb">
	        <div class="pull-left">
	            <h2>Create New Role</h2>
	        </div>
	        <div class="pull-right">
	            <a class="btn btn-primary" href="{{ route('roles') }}"> Back</a>
	        </div>
	    </div>
	</div>

	
	<form method="POST" action="{{ route('roles.store') }}" accept-charset="UTF-8">
        {{ csrf_field() }}
    	<div class="row">

    		<div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input placeholder="Name" class="form-control" name="name" type="text" value="">
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Display Name:</strong>
                    <input placeholder="Display Name" class="form-control" name="display_name" type="text" value="">
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Description:</strong>
                    <textarea name="description" id="" class="form-control" cols="30" rows="10"></textarea>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Permission:</strong>
                    
                    <label for=""><input type="checkbox" name="Permissions[]" value="1"> 权限1</label>
                    <label for=""><input type="checkbox" name="Permissions[]" value="1"> 权限1</label>
                    <label for=""><input type="checkbox" name="Permissions[]" value="1"> 权限1</label>
                    <label for=""><input type="checkbox" name="Permissions[]" value="1"> 权限1</label>
                    <label for=""><input type="checkbox" name="Permissions[]" value="1"> 权限1</label>
                    <label for=""><input type="checkbox" name="Permissions[]" value="1"> 权限1</label>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
    			<button type="submit" class="btn btn-primary">Submit</button>
            </div>

    	</div>
	</form>

</div>
@endsection