<?php

namespace app\Admin\controller;

use think\Db;
use \think\Request;
use think\File;
use think\Paginator;
use think\Session;
use Upyun\Upyun;
use Upyun\Config;
// use app\driver\cache\Redis;

class Picture
{
	/**
	 * [show description]展示图片列表页
	 * @return [type] [description]
	 */
	public function pictureList()
	{
		$limit = input('get.limit', 3);
		$query = new \think\db\Query();
		$query->table('admin_user')->field('openid, admin_name, phone, email, sculpture, upyun_pic, login_count, last_login_ip, last_login_address');
		$count = DB::table('admin_user')->count('id');
		// 获取分页显示
		$info = $query->paginate($limit);
        $page = $info->render();
		// var_dump($info);
		return view('Picture/picture-list', ['info' => $info, 'page' => $page, 'count' => $count]);   
	}

	public function show()
	{
		return view('Picture/picture-add');
	}

	/**
	 * [handle description]处理图片上传到本地再上传到又拍云数据库
	 * @return [type] [description]
	 */
	public function handle()
	{
		$admin_data = Session::get('admin_data');
		// 获取表单上传文件 例如上传了001.jpg
		$files = request()->file('image');
		// $filename = [];
		foreach($files as $file){
			 // 移动到框架应用根目录/uploads/ 目录下
			$info = $file->move(APP_PATH.'uploads/');
			if($info){
			    $filename = $info->getFilename();
			    $dir = APP_PATH.'uploads/'.$info->getSaveName();
			}else{
			    // 上传失败获取错误信息
			    echo $file->getError();
			}
    	}
		// var_dump($filename);die;
	// 20cfd5779cf52c91dc3fa21afa817fa6
		//又拍云服务名
		$serviceName = 'yq-image-cdn';
		//操作员名
		$operatorName = 'yaoqi';
		//操作员密码
		$operatorPassword = '383495167yaoqi';
		//config传入 服务名 操作员名
		$serviceConfig = new Config($serviceName, $operatorName, $operatorPassword);
		$client = new Upyun($serviceConfig);
		$file = fopen($dir, 'r');
		$ymd = date("Y").date("m").date("d");   //文件路径格式
		$save_url= $ymd. "/";     				//优盘云存储路径
		$con = $save_url.$filename;				//优盘云路径加文件名
		// $tasks = array('x-gmkerl-thumb' => '/format/png0');
		$res = $client->write("/".$con, $file);

		$img = 'http://yq-image-cdn.test.upcdn.net/'.$con.'!width200';
		if($res){	
			$upyun_pic = DB::table('admin_user')->where('admin_name', $admin_data['admin_name'])->value('upyun_pic');
			if(!empty($upyun_pic) && $upyun_pic == $filename){
				$client->delete($save_url.$upyun_pic);	
			}
			DB::table('admin_user')
				->where('admin_name', $admin_data['admin_name'])
				->update([
					'sculpture' => $img,
					'upyun_pic' => $filename,
				]);
			
			return json(['data' => '', 'code' => 0, 'msg' => 'success']);
		}

	}



	/**
	* @return   [<description>]
 	* @description 展示信息编辑页面
	*/
 	public function showedit()
	{
		return view('Picture/picture-edit');
		
	}



}
