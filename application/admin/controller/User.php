<?php

namespace app\Admin\controller;

use think\Db;
use think\Session;
use think\Paginator;
use think\cache\driver\Redis;
use think\captcha\Captcha;
use think\image\image;

class User
{
	/**
	 * [index description] 登录页面
	 * @return [type] [description]
	 */
    public function login()
    {
        return view('User/login');
    }
    
    /**
     * @description 显示验证码
     */
    public function showCode()
    {
        $captcha = new Captcha();
        return $captcha->entry();
    }

    /**
     * @description 检查验证码
     */
    protected function check_verify($code, $id = '')
    {
        $captcha = new Captcha();
        return $captcha->check($code, $id);
    }

    /**
     * @description 处理登录
     * @return json 数据 错误码，错误信息 
     */
    public function handleLogin()
    {
        //接收所有参数
        $post_data = input('post.');
        $adminName = $post_data['admin_name'];
        $password = $post_data['password'];
        $code = $post_data['code'];
        //检查验证码是否正确
        $bool = $this->check_verify($code);
        if(!$bool){
            return json(['data' => '', 'code' => -1, 'msg' => '验证码错误']);
        }
        //获取错误次数
    	$redis = new Redis();
    	$errCount = $redis->get('errorCount:'.$adminName);
    	if($errCount==3){
    		return json(['data' => '', 'code' => -1, 'msg' => '登录错误次数超过三次，请两小时后再试']);
    	}
    	$userData = DB::table('admin_user')->where('admin_name', $adminName)->limit(1)->find();
    	//比对密码是否正确
    	$hashpassword = md5($password.$userData['rand_salt']);
		if ($hashpassword == $userData['password']) {
			// 记录登录次数
            $login_count = $userData['login_count'] + 1;
            //把上次登陆时间，登录次数，用户名存入session
            $current_data = [
                'admin_name' => $adminName,
                'last_login' => $userData['last_login'],
                'login_count' => $login_count,
                'last_login_ip' => $userData['last_login_ip']
			];
			// 登录成功，存入用户数据到session
			Session::set('admin_data', $current_data);
			//把本次登录时间记录下来,下一次登录的时候显示
			$last_login = time();
			//登录IP
			$last_login_ip = $this->getIP();
        	//获取登录城市信息
			$cityData = $this->getCity();
			// var_dump($cityData);die;
			if(!isset($cityData) || empty($cityData)){
				$last_login_address = '';
			}else{
				$last_login_address = $cityData['region'].$cityData['city'];

			}
			$data = [
				'login_count' => $login_count,
				'last_login' => $last_login,
				'last_login_ip' => $last_login_ip,
				'last_login_address' => $last_login_address
			];
			//更新到用户表
			DB::table('admin_user')->where('admin_name', $adminName)->update($data);
			//删除错误次数
            // Session::set('admin_name', $adminName);
			$redis->rm('errorCount:'.$adminName);
			return json(['data' => '', 'code' => 0, 'msg' => '登录成功']);
		} else {
			//密码错误记录错误次数
			$count = $redis->inc('errorCount:'.$adminName);	
			//剩余错误次数
			$surplusCount = 3 - $count;
			if($surplusCount >0){
				return json(['data' => '', 'code' => -1, 'msg' => '用户名或密码错误，您还有'.$surplusCount.'次机会']);
			}else{	
				return json(['data' => '', 'code' => -1, 'msg' => '登录错误次数超过三次，请两小时后再试']);
			}
		}
    }
    
    /**
     * [loginOut description] 退出登录
     * @return [type] [description]
     */
    public function loginOut()
    {
		// var_dump(phpinfo());die;
    	Session::delete('admin_data');
    	// var_dump(Session::get('admin_name'));die;	
    	return view('User/login');
    }

	public function add_userinfo()
	{
		require_once THINK_PATH.'library/qiniu/qqConnectAPI.php';
	}

    /**
     * [getCity description] 获取城市信息
     * @param  string $ip [description]
     * @return [type]     [description]
     */
    protected function getCity($ip = '')
	{
	    if($ip == ''){
	        $data = $ip;
	    }else{
	        $url = "http://ip.taobao.com/service/getIpInfo.php?ip=".$ip;
	        $ip = json_decode(file_get_contents($url), true);   
	        if((string)$ip['code'] == '1'){
	           return false;
	        }
	    }
	    return $data;   
	}

	/**
	 * [getIP description] 获取客户端真实ip地址
	 * @return [type] [description]
	 */
	protected function getIP()
	{
	    static $realip;
	    if (isset($_SERVER)){
	        if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
	            $realip = $_SERVER["HTTP_X_FORWARDED_FOR"];
	        } else if (isset($_SERVER["HTTP_CLIENT_IP"])) {
	            $realip = $_SERVER["HTTP_CLIENT_IP"];
	        } else {
	            $realip = $_SERVER["REMOTE_ADDR"];
	        }
	    } else {
	        if (getenv("HTTP_X_FORWARDED_FOR")){
	            $realip = getenv("HTTP_X_FORWARDED_FOR");
	        } else if (getenv("HTTP_CLIENT_IP")) {
	            $realip = getenv("HTTP_CLIENT_IP");
	        } else {
	            $realip = getenv("REMOTE_ADDR");
	        }
	    }
	    return $realip;
	}

}
