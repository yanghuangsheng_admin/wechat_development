<?php

namespace app\Admin\controller;

use think\Db;
use think\Request;
use think\Paginator;
use think\Session;
// use app\driver\cache\Redis;

class Article
{
    /**
     * @ description 博文数据列表
     * @ return 视图 文章数据，分页数据
     */
    public function articleList()
    {
        //检查用户状态
        if(empty(Session::get('admin_data'))){
            return view('User/login');
        }else{
            $keyword = input('get.keyword');
            //接收每页显示数据条数,默认每页显示3条
            $limit = input('get.limit', 3);
            $query = new \think\db\Query();
            $query->table('article_base_info');
            $map = [];
            if(!empty($keyword)){
                $map['title'] = ['like', '%'.$keyword.'%'];
                $query->where($map);
            }
            $count = DB::table('article_base_info')->count('id');
            //返回分页数据
            $list_data = $query->order('id desc')->paginate($limit);
            $page = $list_data->render();
            // var_dump($count);die;
            return view('Article/article-list', ['list_data' => $list_data, 'page' => $page, 'count' => $count]);
        }
    }
    
    /**
     * @ description 文章详情
     * @ return 文章详情页面 文章数据
     */
    public function show($id)
    {   
        //检查用户状态
        if(empty(Session::get('admin_data'))){
            return view('User/login');
        }else{
            //连表查询数据
            $detail = DB::table('article_base_info')->join('article', 'article_base_info.id=article.article_id', 'left')->where('article_base_info.id', $id)->limit(1)->find();
            //返回视图和数据
            return view('Article/article_show', ['detail' => $detail]);
        }
    }

    /**
     * @ description 改变文章状态
     * @ return $bool 成功或失败的状态码
     */
    public function changeStatus()
    {
        //检查用户状态
        if(empty(Session::get('admin_data'))){
            return view('User/login');
        }else{
            //获取文章id
            $id = input('get.id');
            //获取操作类型
            $type = input('get.type');
            //type为1时上架
            if($type==1){
                $bool = DB::table('article_base_info')->where('id', $id)->setField('is_hot', 1); 
                if($bool){
                    return json(['data' => '', 'code' => $bool, 'msg' => '操作成功']);  
                }
                //type为0时下架
            }else{
                $bool = DB::table('article_base_info')->where('id', $id)->setField('is_hot', 0);   
                if($bool==0){
                    return json(['data' => '', 'code' => $bool, 'msg' => '操作失败']);
                }
            }
        }
    }

    /**
     * [delete description]删除数据
     * @return [type] [description]
     */
    public function delete()
    {
        //检查用户状态
        if(empty(Session::get('admin_data'))){
            return view('User/login');
        }else{
            $id = (int)input('get.id');
            // dump($id);die;
            //开启事务
            DB::startTrans();
            try{
                $bool_base = DB::table('article_base_info')->where('id', $id)->delete();
                $bool_content = DB::table('article')->where('article_id', $id)->delete();
                if($bool_base && $bool_content){
                    DB::commit();
                    return json(['data' => '', 'code' => 0, 'msg' => '删除成功']);
                }
            }catch(\Exception $e){
                DB::rollback();
                return json(['data' => '', 'code' => 0, 'msg' => '删除失败']);
            }
        }
        // dump($bool);die;
    }

    /**
     * @ description 显示要修改的数据
     * @ return 编辑页面视图 分类数据，要修改的数据
     */
    public function edit($id)
    {
        //检查用户状态
        if(empty(Session::get('admin_data'))){
            return view('User/login');
        }else{
            //查询全部分类数据
            $cate_data = DB::table('article_cate')->field('id, cate_name')->select();
            // var_dump($cate_data);
            //根据文章id查询要修改的数据
            $data = DB::table('article_base_info')
            ->join('article', 'article_base_info.id=article.article_id', 'left')
            ->where('article_base_info.id', $id)->limit(1)->find();
            return view('Article/article-edit', ['data' => $data, 'cate_data' => $cate_data]);
        }
    }

    /**
     * @ description 执行数据修改
     * @ return 成功状态码
     */
    public function update()
    {
        //检查用户状态
        if(empty(Session::get('admin_data'))){
            return view('User/login');
        }else{
            $post_data = input('post.');
            //文章id
            $id = $post_data['id'];
            //文章作者
            $author = $post_data['author'];
            //文章标题
            $title = $post_data['articletitle'];
            //分类id
            $cate_id = $post_data['cate_id'];
            //文章内容
            $content = $post_data['content'];
            //根据分类id查询对应分类名称
            $cate_data = DB::table('article_cate')->where('id', $cate_id)->field('cate_name')->limit(1)->find();
            $cate = $cate_data['cate_name'];
            //插入基本信息表的数据
            $base_data = [
                'title' => $title,
                'cate' => $cate,
                'cate_id' => $cate_id,
                'author' => $author,
                'is_hot' => 1,
                'create_time' => time()
            ];
            //插入文章表数据
            $content_data = [
                'cate_id' => $cate_id,
                'content' => $content
            ];
            //没有任何修改时bool值为0
            //有修改时时返回影响行数bool值为1
            $bool_base = DB::table('article_base_info')->where('id', $id)->update($base_data);
            $bool_content = DB::table('article')->where('article_id', $id)->update($content_data);
            return json(['data' => '', 'code' => 0, 'msg' => '修改成功']);
        }
        
    }

    /**
     * @ description 显示新增文章页面 
     */
    public function add($type)
    {
        //检查用户状态
        if(empty(Session::get('admin_data'))){
            return redirect(url('User/login'));
        }else{
            // type为1时展示添加文章页面
            if($type==1){
                //查询分类数据
                $cate_data = DB::table('article_cate')->field('id, cate_name')->select();
                return view('Article/article-add', ['cate_data' => $cate_data]);
            }
            // type为2时保存添加的文章数据
            if($type==2){
                $post_data = input('post.');
                //获取分类id
                $cate_id = $post_data['cate_id'];
                $author = $post_data['author'];
                $title = $post_data['title'];
                $content = $post_data['content'];
                //检查文章是否存在
                $res = DB::table('article_base_info')->where('title', $title)->field('title')->limit(1)->find();
                if(!empty($res)){
                    return json(['data' => '', 'code' => -1, 'msg' => '文章已存在，不能重复添加']);
                }
                //查询分类名称
                $cate_data = DB::table('article_cate')->where('id', $cate_id)->field('cate_name')->limit(1)->find();
                $cate_name = $cate_data['cate_name'];
                $base_info = [
                    'title' => $title,
                    'cate' => $cate_name,
                    'cate_id' => $cate_id,
                    'author' => $author,
                    'create_time' => time()
                ];
                // 启动事务
               /* DB::startTrans();
                try{*/
                    //插入数据文章基本数据，并获取新增数据的id
                    $id = DB::table('article_base_info')->insertGetId($base_info);
                    //插入文章
                    $content_data = [
                        'article_id' => $id,
                        'cate_id' => $cate_id,
                        'content' => $content
                    ];
                    $bool = DB::table('article')->insert($content_data);
                    // var_dump($bool);die;
                    if($bool==1){
                        return json(['data' => '', 'code' => 0, 'msg' => '添加成功']);
                    }
                  /*  DB::commit();
                }catch(\Exception $e){
                    // 回滚事务
                    DB::rollback();*/
                // }
            }
        }
            

    }

    
}
