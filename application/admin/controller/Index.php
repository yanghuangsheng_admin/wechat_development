<?php

namespace app\Admin\controller;

use think\Db;
use think\Session;
use think\Paginator;
use think\Controller;
use think\cache\driver\Redis;

class Index
{
    /**
     * @description 后台首页
     */
    public function index()
    {
        //检查session
        if(empty(Session::get('admin_data'))){
            return view('User/login');
        }else{
            return view('Index/index');
        }       
    }

    /**
     * @description 后台首页欢迎页面
     */
    public function welcome()
    {
        return view('Index/welcome');
    }
}