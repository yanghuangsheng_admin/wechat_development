<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
/** 打印数据
 * @param $data mixed 数据
 * @param string $filename 文件名
 * @param string $title 标题
 * @param string $color 颜色
 */
function log_test($data,$filename = 'hls',$title = '打印',$color= '')
{
    $time = time();
    // 保证不要出错
    if(!is_string($data)) $data = var_export($data, true);
    if(!is_string($filename)) $filename = 'txt';
    if(!is_string($title)) $title = '打印';
    if(!is_string($color)) $color = '';

    // 自定义颜色
    if($color == 1) $color = 'red';
    if($color == 2) $color = 'green';
    if($color == 3) $color = 'blue';
    if(!empty($color)) $data = "<span style='color:".$color."'>".$data."</span>";

    // 判断路径
    $path = RUNTIME_PATH . "/".$filename.".txt";
    if(!file_exists($path)) touch($path);// 不存在就返回
    // 写入
    file_put_contents($path, "------------------".$time." start------------------\r\n</br>" . "标题:".$title."---时间:".date('Y-m-d H:i:s')." \r\n</br>" . PHP_EOL . $data . "\r\n</br>------------------$time end--------------------</br></br>", FILE_APPEND);
}

/**
 * @param $url
 * @param $params
 * @param string $method
 * @param array $header
 * @param bool $multi
 * @return mixed
 * @throws Exception
 */
function http($url, $params, $method = 'GET', $header = array(), $multi = false)
{
    $opts = array(
        CURLOPT_TIMEOUT => 30,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_SSL_VERIFYHOST => false,
        CURLOPT_HTTPHEADER => $header,
        CURLOPT_HEADER => 0,
    );
    /* 根据请求类型设置特定参数 */
    switch (strtoupper($method)) {
        case 'GET':
            $opts[CURLOPT_URL] = $url . '?' . http_build_query($params);
            break;
        case 'POST':
            //判断是否传输文件
            $params = $multi ? $params : http_build_query($params);
            $opts[CURLOPT_URL] = $url;
            $opts[CURLOPT_POST] = 1;
            $opts[CURLOPT_POSTFIELDS] = $params;
            break;
        default:
            throw new Exception('不支持的请求方式！');
    }
    /* 初始化并执行curl请求 */
    $ch = curl_init();
    curl_setopt_array($ch, $opts);
    $data = curl_exec($ch);
    $error = curl_error($ch);
    curl_close($ch);
    if ($error) {
        throw new Exception('请求发生错误：' . $error);
    }

    return $data;
}



/**
 * [debug 调试输出函数]
 * @param  [mix]  $val  [调试输出源]
 * @param  [bool]  $dump [是否启用var_dump调试]
 * @param  boolean $exit [是否在调试结束后设置断点]
 * @return        [void]
 */
function debug($val,$dump=flase,$exit=true){
    //自动或区域调试函数名称$func
    if($dump){
        $func = 'var_dump';
    }else{
        $func = (is_array($val) || is_object($val)) ? 'print_r' : 'printf';
    }

    //输出到html
    header("Content-type:text/html;charset=utf-8");
    echo "<pre>debug output:<hr/>";
    $func($val);
    echo '</pre>';
    if($exit) exit;
}

/**
 * @param $url
 * @param string $method 'post' or 'get'
 * @param null $postData
 *      类似'para1=val1&para2=val2&...'，
 *      也可以使用一个以字段名为键值，字段数据为值的JSON数据。
 *      如果value是一个数组，Content-Type头将会被设置成multipart/form-data
 *      从 PHP 5.2.0 开始，使用 @ 前缀传递文件时，value 必须是个数组。
 *      从 PHP 5.5.0 开始, @ 前缀已被废弃，文件可通过 \CURLFile 发送。
 * @param array $options
 * @return mixed
 */
function curl_execute($url, $method, $postData = null, $options = array(), &$errors = array())
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 150); //设置cURL允许执行的最长秒数
    //https请求 不验证证书和host
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    if (strtolower($method) === 'post') {
        curl_setopt($ch, CURLOPT_POST, true);
        if ($postData !== null) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        }
    }
    if (!empty($options)) {
        curl_setopt_array($ch, $options);
    }
    if (!($output = curl_exec($ch))) {
        $errors = array(
                'errno' => curl_errno($ch),
                'error' => curl_error($ch),
            ) + curl_getinfo($ch);
    }
    curl_close($ch);
    return $output;
}

/**
 * @param $url
 * @return mixed
 */
function get($url)
{
    return curl_execute($url, 'get');
}

/**
 * @param $url
 * @param $postData
 * @return mixed
 */
function post($url, $postData)
{
    return curl_execute($url, 'post', $postData);
}

/**
 * @param $url url
 * @param $field 字段名
 * @param $filename 文件名
 * @param array $postData
 * @return mixed
 */
function file_upload($url, $field = 'media', $filename, $postData = array())
{
    $filename = realpath($filename);
    //PHP 5.6 禁用了 '@/path/filename' 语法上传文件
    if (class_exists('\CURLFile')) {

        $postData[$field] = new \CURLFile($filename);
    } else {

        $postData[$field] = '@' . $filename;
    }

    return curl_execute($url, 'post', $postData);
}

/**
 * 获取access_token
 * @return mixed
 * @throws \Exception
 */
function getAccessToken()
{
    $url = "https://api.weixin.qq.com/cgi-bin/token";
    $params = [
        'grant_type' => 'client_credential',
        'appid' => APPID,
        'secret' => APPSECRET,
    ];
    $redis = new \Redis();
    $redis->connect(REDIS_HOST);
    //判断redis中是否存在access_token
    if (!$redis->exists('access_token')) {
        $result = http($url, $params);
        log_test($result);

        $result = json_decode($result, true);
        if (isset($result['errcode'])) {
            log_test('获取access_token失败' . PHP_EOL . '错误信息：' . $result['errmsg']);
            exit();
        }

        $redis->setex('access_token', $result['expires_in'] - 100, $result['access_token']);
        $access_token =  $result['access_token'];
    } else {
        $access_token = $redis->get('access_token');
    }

    return $access_token;
}

