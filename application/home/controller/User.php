<?php

namespace app\Home\controller;

use think\Db;
use think\Session;
use think\Paginator;
use think\cache\driver\Redis;
use \Qcloud\Sms\SmsSingleSender;
use think\Request;
use think\captcha\Captcha;
use app\common\ConnectRedis;
use phpmialer\phpmialer\PHPMailer;
use phpmialer\phpmialer\Exception;

class User
{
	/**
	 * [show description] 展示发送邮件页面
	 * @return [type] [description]
	 */
	public function show()
	{
		return view('Login/fogetPass');
	}

    public function see()
    {
        return view('Login/fogetPass.bak');
    }

	/**
     * @description 显示验证码
     */
    public function showCode()
    {
        $captcha = new Captcha();
        return $captcha->entry();
    }

    /**
     * @description 检查验证码
     */
    protected function check_verify($code, $id = '')
    {
        $captcha = new Captcha();
        return $captcha->check($code, $id);
    }

    /**
     * [checkEmail description] 检查邮箱和验证码
     * @return [type] [description]
     */
    public function checkEmail()
    {   
        $type = input('post.type');
        $email = input('post.email');
        $code = input('post.code');
        // var_dump($code);
        // var_dump($email);die;
        //type为1时检查邮箱
        if($type==1){ 
            $data = DB::table('admin_user')->where('email', $email)->limit(1)->find();
            // var_dump($data);die;
            if(empty($data)){
                return json(['data' => '', 'code' => '-1', 'msg' => '邮箱不存在']);
            }
        }

    }

    /**
     * [send description]执行邮件的发送
     * @return [type] [description]
     */
    public function send()
    {
        //接受邮箱和验证码参数
        $email = input('post.email');
        $code = input('post.code');
        //检查验证码
        $bool = $this->check_verify($code);
        if(!$bool){
            return json(['data' => '', 'code' => -1, 'msg' => '验证码错误']);
        }else{
            //发送邮件
            $res = $this->email($email);
            if($res){
                return json(['data' => '', 'code' => 0, 'msg' => '邮件已发送，请注意查收']);
            }
        }
    
    
    }
    
    /**
     * tp5邮件
     * @param
     * @author staitc7 <static7@qq.com>
     * @return mixed
     */
    public function email($email)
    {

        // $toemail='1664686517@qq.com';
        $toemail = $email;
        $name='yaoqi';
        $subject='重置密码';
        $content = file_get_contents('http://www.yaoqilife.top/home/User/Login/see');
        $bool = send_mail($toemail,$name,$subject,$content);
		return $bool;	
    }




	
}
