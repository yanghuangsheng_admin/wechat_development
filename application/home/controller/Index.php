<?php

namespace app\Home\controller;

use think\Db;
use think\Session;
use think\Env;
use think\Paginator;
use think\cache\driver\Redis;
// use Lixunguan\Yuntongxun\Sdk as Yuntongxun;
use \Qcloud\Sms\SmsSingleSender;
use think\Request;
use app\common\ConnectRedis;
use phpmialer\phpmialer\PHPMailer;
use phpmialer\phpmialer\Exception;

class Index
{
	/**
	 * [index description] 首页面
	 * @return [type] [description]
	 */
    public function index($cate_id=0)
    {
        $keyword = input('get.keyword');
        //接收每页显示数据条数,默认每页显示3条
		$limit = input('get.limit', 3);
        // 使用Query对象查询
        $query = new \think\db\Query();
		$query->table('article_base_info');
		$map = [];
        if(!empty($keyword)){
            $map['title'] = ['like', '%'.$keyword.'%'];
            $query->where($map);
		}
		if($cate_id != 0){
			$map['cate_id'] = $cate_id;
			$query->where($map);
		}
			
        // 获取分页显示
		$info = $query->paginate($limit);
        $page = $info->render();
        return view('Login/index', ['info' => $info, 'page' => $page]);
    }

    /**
     * [details description] 文章详情列表
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function details($id)
    {
        $list_data = DB::table('article_base_info')->join('article', 'article_base_info.id=article.article_id', 'left')->where('article_base_info.id', $id)->limit(1)->find();
    	$comment_data = DB::table('comment')->where('article_id', $list_data['article_id'])->select();
    	return view('Login/details', ['list_data' => $list_data, 'comment_data' => $comment_data]);
    }

    /**
     * [hot description] 侧边栏数据
     * @return [type] [description]
     */
    public function hot()
    {
        $hot_data = DB::table('article_base_info')->where('is_hot', 1)->field('id, title')->select();
        foreach($hot_data as &$v){
            $v['url'] = url('Login/details', ['id'=> $v['id']]);
        }
        // var_dump($hot_data);die;
        return json(['data' => $hot_data, 'code' => 0, 'msg' => '']);
	}
	
	/**
	 * @description 菜单栏数据
	 */
	public function menu()
	{
		$cate_data = DB::table('article_cate')->field('id, cate_name')->select();
		foreach($cate_data as &$v){
			$v['url'] = url('Login/index', ['cate_id' => $v['id']]);
		}
		// var_dump($cate_data);die;
		return json(['data' => $cate_data, 'code' => 0, 'msg' => '']);
	}

	/**
	 * [register description] 注册页面
	 * @return [type] [description]
	 */
    public function register()
    {
        return view('Login/register');
    }

    /**
     * [login description] 登录页面
     * @return [type] [description]
     */
    public function login()
    {
        return view('Login/login');
	}
    
    /**
     * [handle description]处理注册
     * @return [type] [description]
     */
    public function handle($type=1, $openid = '', $nickname = '')
    {
		if($type==1){
			//接受参数
			$adminName = trim(input('post.admin_name'));
			$password = input('post.password');
			$phone = input('post.phone');
			$code = input('post.code');
			//检查用户是否存在
			$userData = DB::table('admin_user')->where('admin_name', $adminName)->limit(1)->find();
			//检查手机是否存在
			$checkPhone = DB::table('admin_user')->where('phone', $phone)->limit(1)->find();
			// var_dump($checkphone);die;
			if(!empty($userData)){
				return json(['data' => '', 'code' => -1, 'msg' => '用户名已存在']);
			}
			if(!empty($checkPhone)){
				return json(['data' => '', 'code' => -2, 'msg' => '手机号已被注册']);
			}
			$bool = $this->checkCode($phone, $code);
			if($bool){
				$rand_salt = rand(1, 9999999);
				// 获取客户端注册真实ip地址
				$ip = $this->getIP();
				//获取城市信息
				$cityData = $this->getCity();
				$ip_address = $cityData['province'].$cityData['city'];
				$create_time = time();
				//加密处理
				$hashpassword = md5($password.$rand_salt);
				//插入数据
				$data = [
					'admin_name' => $adminName, 
					'password' => $hashpassword, 
					'phone' => $phone,
					'rand_salt' => $rand_salt, 
					'ip' => $ip,
					'ip_address' => $ip_address,
					'create_time' => $create_time
				];
				$bool = DB::table('admin_user')->insert($data);
				if($bool==1){
					$redis = new ConnectRedis();
     				$redis = $redis->getClient(2);
     				//注册成功，清楚验证码和短信发送次数
     				$redis->del('code|'.$phone, 'clickNum|'.$phone);
     				Session::set('admin_name', $adminName);
					return json(['data' => '', 'code' => 0, 'msg' => '注册成功']);
				}
			}
		}else{
			//qq方式登录，若用户不存在，就注册一个账号
			// 获取客户端注册真实ip地址
			$ip = $this->getIP();
			//获取城市信息
			$cityData = $this->getCity();
			$ip_address = $cityData['province'].$cityData['city'];
			$create_time = time();
			$data = [
				'openid' => $openid,
				'admin_name' => $nickname,
				'ip'	=> $ip,
				'login_count' => 1,
				'ip_address' => $ip_address,
				'type' =>2,    //qq方式
				'create_time' => $create_time
			];
			$data = DB::table('admin_user')->insert($data);
			// $user_data = [$nickname, $figureurl];
			Session::set('admin_name', $nickname);
			return true;
		}
    }

    /**
     * [dologin description] 执行登录操作
     * @return [type] [description]
     */
    public function dologin($type=1, $openid='', $nickname='')
    {
		$redis = new Redis();
		// var_dump($redis);die;
		if($type==1){
			//接受参数
			$adminName = input('post.admin_name');
			$password = input('post.password');
			$userInfo = DB::table('admin_user')->where('admin_name', $adminName)->find();
			if(empty($userInfo)){
    			return json(['data' => '', 'code' => -1, 'msg' => '用户名或密码错误']);
    		}
			//获取错误次数
			$errCount = $redis->get('errorCount:'.$adminName);
			if($errCount==3){
				return json(['data' => '', 'code' => -1, 'msg' => '登录错误次数超过三次，请两小时后再试']);
			}
			$userData = DB::table('admin_user')->where('admin_name', $adminName)->find();
			//比对密码是否正确
			$hashpassword = md5($password.$userData['rand_salt']);
			if ($hashpassword == $userData['password']) {
				// 记录登录次数
				$login_count = $userData['login_count'] + 1;
				//登录时间
				$last_login = time();
				//登录IP
				$last_login_ip = $this->getIP();
				//获取登录城市信息
				$cityData = $this->getCity();
				$last_login_address = $cityData['province'].$cityData['city'];
				$data = [
					'login_count' => $login_count,
					'last_login' => $last_login,
					'last_login_ip' => $last_login_ip,
					'last_login_address' => $last_login_address
				];
				//更新到用户表
				DB::table('admin_user')->where('admin_name', $adminName)->update($data);
				//登录成功，存入用户名到redis和session,删除错误次数
				Session::set('admin_name', $adminName);
				$redis->rm('errorCount:'.$adminName);
				return json(['data' => '', 'code' => 0, 'msg' => '登录成功']);
			} else {
				//密码错误记录错误次数
				$count = $redis->inc('errorCount:'.$adminName);	
				//剩余错误次数
				$surplusCount = 3 - $count;
				if($surplusCount >0){
					return json(['data' => '', 'code' => -1, 'msg' => '用户名或密码错误，您还有'.$surplusCount.'次机会']);
				}else{	
					return json(['data' => '', 'code' => -1, 'msg' => '登录错误次数超过三次，请两小时后再试']);
				}
			}
		}else{
			$userData = DB::table('admin_user')->where('openid', $openid)->limit(1)->find();
			// 记录登录次数
			$login_count = $userData['login_count'] + 1;
			//登录时间
			$last_login = time();
			//登录IP
			$last_login_ip = $this->getIP();
			//获取登录城市信息
			$cityData = $this->getCity();
			$last_login_address = $cityData['province'].$cityData['city'];
			$data = [
				'login_count' => $login_count,
				'last_login' => $last_login,
				'last_login_ip' => $last_login_ip, 
				'last_login_address' => $last_login_address
			];
			//更新到用户表
			DB::table('admin_user')->where('openid', $openid)->update($data);
			// $user_data = [$nickname, $figureurl];
			// Session::set('admin_name', $nickname);
			return true;
		}
    }

    /**
     * [loginOut description] 退出登录
     * @return [type] [description]
     */
    public function loginOut()
    {
    	Session::delete('admin_name');
    	Session::delete('figureurl');
    	// var_dump(Session::get('admin_name'));die;	
    	return view('Login/login');
    }

    /**
     * [getCity description] 获取城市信息
     * @param  string $ip [description]
     * @return [type]     [description]
     */
    protected function getCity($ip = '')
	{
	    if($ip == ''){
	        // $url = "http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=json";
	        $url = "http://ip.taobao.com/service/getIpInfo.php?ip=".$ip;
	        // $ip = json_decode(file_get_contents($url),true);
	        $data = $this->curl_get_https($url);
	        $data = json_decode($ip, true)['ip'];
	    }else{
	        $url = "http://ip.taobao.com/service/getIpInfo.php?ip=".$ip;
	        $ip = json_decode(file_get_contents($url));   
	        if((string)$ip->code == '1'){
	           return false;
	        }
	        $data = (array)$ip->data;
	    }
	    return $data;   
	}

	/**
	 * [getIP description] 获取客户端真实ip地址
	 * @return [type] [description]
	 */
	protected function getIP()
	{
	    static $realip;
	    if (isset($_SERVER)){
	        if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
	            $realip = $_SERVER["HTTP_X_FORWARDED_FOR"];
	        } else if (isset($_SERVER["HTTP_CLIENT_IP"])) {
	            $realip = $_SERVER["HTTP_CLIENT_IP"];
	        } else {
	            $realip = $_SERVER["REMOTE_ADDR"];
	        }
	    } else {
	        if (getenv("HTTP_X_FORWARDED_FOR")){
	            $realip = getenv("HTTP_X_FORWARDED_FOR");
	        } else if (getenv("HTTP_CLIENT_IP")) {
	            $realip = getenv("HTTP_CLIENT_IP");
	        } else {
	            $realip = getenv("REMOTE_ADDR");
	        }
	    }
	    return $realip;
	}

	/**
	 * qq登录方式
	 */
	public function Qlogin()
	{
		
		require_once THINK_PATH.'library/connect/API/qqConnectAPI.php';
		//访问QQ的登录页面  
		$oauth = new \QC();  
		
		$oauth->qq_login();die;
		
	} 

	//获取access_token和openid
	/**
	 * [get_code_state description] qq登录回调地址
	 * @param  [type] $code  [description]
	 * @param  [type] $state [description]
	 * @return [type]        [description]
	 */
	public function get_code_state($code, $state)
	{
		//引入QQAPI
		require_once THINK_PATH.'library/connect/API/qqConnectAPI.php';
		$qc = new \QC();
		//获取access_token
		$acce = $qc->qq_callback();
		// 获取openid
		$openid = $qc->get_openid();
		$qq = new \QC($acce, $openid);
		//调用获取用户信息接口
		$arr = $qq->get_user_info();
		//获取昵称
		$nickname = $arr['nickname'];
		//获取头像
		$figureurl = $arr['figureurl'];
		$res = DB::table('admin_user')->where('openid', $openid)->limit(1)->find();
		//如果存在此用户，则用账号去登录，并存基本信息到session
		if(!empty($res)){
			$bool = $this->dologin($type=2, $openid, $nickname);
			if($bool){
				// var_dump($openid);die;
				Session::set('admin_name', $nickname);
				Session::set('figureurl', $figureurl);
				//登录成功重定向到首页
				return redirect(url('Login/index'));
			}
		}else{
			//如果数据库不存在此用户，用昵称和openid到站点注册，完了存session并直接登录
			$bool = $this->handle($type=2, $openid, $nickname);
			if($bool){
			// var_dump($bool);die;
				Session::set('admin_name', $nickname);
				Session::set('figureurl', $figureurl);
				//登录成功重定向到首页
				return redirect(url('Login/index'));
			}
		}
	}

    // 检查验证码
    private function checkCode($phone, $code)
    {
    	//实例化链接redis实例
    	$redis = new ConnectRedis();
     	$redis = $redis->getClient(2);
    	//获取session中存储的验证码
    	$CacheCode = $redis->get('code|'.$phone);
    	// $SessCode = 23453;
    	if($CacheCode != $code){
    		return json(['data' => '', 'code' => -1, 'msg' => '验证码错误']);
    	}else if(empty($CacheCode)){
    		return json(['data' => '', 'code' => -1, 'msg' => '验证码已过期，请重新获取']);
    	}
    	return true;
    }

    /**
     * [getCode description] 获取短信验证码
     * @return [type] [description] json数据
     */
    public function getCode()
    {
    	$phone = input('post.phone');
    	$res = DB::table('admin_user')->where('phone', $phone)->limit(1)->find();
    	//判断用户手机号
    	if(!empty($res)){
    		return json(['data' => '', 'code' => -1, 'msg' => '该手机号已存在']);
    	}
    	//实例化链接redis实例
    	$redis = new ConnectRedis();
     	$redis = $redis->getClient(2);
    	// $redis = new Redis();
    	// 每次点击增加点击次数
    	$redis->incr('clickNum|'.$phone);
    	//设置限制次数的过期时间
    	$redis->expire('clickNum|'.$phone, 3600*24);
    	$clickNum = $redis->get('clickNum|'.$phone);
    	//如果达到限定次数报错
    	if($clickNum >= 3){
    		return json(['data', 'code' => -1, 'msg' => '您的短信发送次数今日已达上限，请24小时后再试']);
    	}
    	// var_dump('1111');die;
    	$appid = '1400060525';
    	$appkey = '6e1790f621e70da002b8df4ad4bad532';
    	$randCode = rand(10000, 99999);
    	$time = 3;
    	//模板参数
    	$params = [$randCode, $time];
    	//模板id
    	$templId = 74016;
		$sender = new SmsSingleSender($appid, $appkey);
		$result = $sender->sendWithParam("86", $phone, $templId, $params, "", "", "");
		$jsonArr = json_decode($result, true);
		if($jsonArr['result'] == 0){
			//验证码发送成功，存入redis，并设置失效时间
			$redis->setex('code|'.$phone, 3600*24, $randCode);
			return json(['data' => '', 'code' => 0, 'msg' => '短信已发送，请注意查收']);
		}else{
			return json(['data' => '', 'code' => -1, 'msg' => '短信发送失败，请重新获取']);
		}	
	}
	
	/**
     * tp5邮件
     * @param
     * @author staitc7 <static7@qq.com>
     * @return mixed
     */
    public function email()
    {

        // $toemail='1664686517@qq.com';
        $toemail = input('post.mail');
        $name='yaoqi';
        $subject='QQ邮件发送测试';
        $content='恭喜你，邮件测试成功。';
        $bool = send_mail($toemail,$name,$subject,$content);
		var_dump($bool);die;	
    }

    public function curl_get_https($url)
    {
        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
        // curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, true);  // 从证书中检查SSL加密算法是否存在
        $tmpInfo = curl_exec($curl);     //返回api的json对象
        //关闭URL请求
        curl_close($curl);
        return $tmpInfo;    //返回json对象
    }




	
}
