<?php 

namespace Admin\Model;
use Think\Model;

class UserModel extends Model
{
	//定义验证规则
	protected $_validate = array(     
		array('name','require','用户名不能为空'), //默认情况下用正则进行验证    
		array('name','','帐号名称已经存在！',0,'unique',1), //在新增的时候验证name字段是否唯一     
		array('password','require','密码不能为空'), //默认情况下用正则进行验证    
		// array('repwd','pwd','确认密码不正确',0,'confirm'), // 验证确认密码是否和密码一致   
		array('email', 'require', '邮箱不能为空'),
		array('email', '', '邮箱已存在', 'unique', 3),  
		array('email', 'email', '邮箱格式不正确'),
		// array('email','checkemail','邮箱不正确',0,'function', 3), // 回调函数
	);


	//定义自动完成规则
	protected $_auto = array (          
		array('money','100000000'),  // 新增的时候把status字段设置为1         
		array('pwd','md5',3,'function') , // 对password字段在新增和编辑的时候使md5函数处理               
		array('addtime','time',3,'function'), // 对update_time字段在更新的时候写入当前时间戳    
	);

	// //自定义验证回调方法
	// public function checkemail($val)
	// {
	// 	// echo $val;
	// 	//写一大堆的验证规则
	// 	if($val != 'admins') { 
	// 		return false;
	// 	} else { 
	// 		return true;
	// 	}
	// }



}
