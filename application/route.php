<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\Route;

Route::get('hello', function () {
    return 'hello world';
});
//公众号开发
Route::group('wechat', function () {

    Route::any('checkSignature','wechat/index/index'); //验证公众号信息
    //Route::get('access_token','wechat/index/getAccessToken'); //获取微信开发的access_token值


    Route::get('addButton','wechat/menu/addButton'); //增加自定义菜单
    Route::get('readButton','wechat/menu/readButton'); //查询自定义菜单
    Route::get('deleteButton','wechat/menu/deleteButton'); //删除自定义菜单

    //获取二维码生成票据
    Route::get('getTicket','wechat/code/getTicket');
    //获取临时二维码
    Route::get('getCode','wechat/code/getCode');
    //获取永久二维码
    Route::get('getPerpetualCode','wechat/code/getPerpetualCode');

    //模板消息
    Route::get('sendTemplate','wechat/template/testSendTemplate');//测试发送模板消息
    //微信跳转授权链接
    Route::any('getBaseInfo','wechat/Auth/getBaseInfo');
    //微信跳转授权回调链接
    Route::any('getUserInfo','wechat/Auth/getUserInfo');
});


Route::get('token','index/user/login'); //获取jwt的token值
Route::any('buy', 'index/index/buy');//秒杀商品


//获取日志内打印的数据
Route::get('get_log',function () {
    // 判断路径
    $path = RUNTIME_PATH . "/hls.txt";
    if(!file_exists($path)) return '';// 不存在就返回
    return file_get_contents($path);
});
//删除日志文件
Route::get('delete_log',function () {
    // 判断路径
    $path = RUNTIME_PATH . "/hls.txt";
    if(!file_exists($path)) return '';// 不存在就返回
    unlink($path);
    return "删除成功";
});



