<?php

namespace app\wechat\service;

use think\Request;
use think\Log;
use app\wechat\controller\Template;

/**
 * 用来返回微信接口数据
 *
 * @Description
 * @author hls
 * @since
 * @Email 1160957956@qq.com
 */
class Wechat
{
    public function message($xmlStr)
    {
//        <xml>
//          <ToUserName><![CDATA[toUser]]></ToUserName> 开发者微信号
//          <FromUserName><![CDATA[FromUser]]></FromUserName> 发送方帐号（一个OpenID）
//          <CreateTime>123456789</CreateTime> 消息创建时间 （整型）
//          <MsgType><![CDATA[event]]></MsgType> 消息类型，event
//          <Event><![CDATA[subscribe]]></Event> 事件类型，subscribe(订阅)、unsubscribe(取消订阅)
//        </xml>
        //将xml数据转换为object
        log_test($xmlStr);
        $postObject = simplexml_load_string($xmlStr);
//        $postObject->ToUserName = '';
//        $postObject->FromUserName = '';
//        $postObject->CreateTime = time();
//        $postObject->MsgType = '';
//        $postObject->Event = '';
        //对参数进行判断
        if ($postObject->MsgType == 'event') {
            $this->reponseEvent($postObject);
        }

    }

    /**
     * 处理事件消息
     * @param object $postObject
     * @return void
     */
    public function reponseEvent($postObject)
    {

        //判断事件消息类型--回复用户消息
        if (strtolower($postObject->Event) == 'subscribe') {
            //订阅
            $toUser = $postObject->FromUserName;
            $fromUser = $postObject->ToUserName;
            $time = time();
            $msgType = 'text';
            $content = '欢迎关注我的公众号';
            $template = "<xml>
                          <ToUserName><![CDATA[%s]]></ToUserName>
                          <FromUserName><![CDATA[%s]]></FromUserName>
                          <CreateTime>%s</CreateTime>
                          <MsgType><![CDATA[%s]]></MsgType>
                          <Content><![CDATA[%s]]></Content>
                        </xml>";
            if (!empty($postObject->EventKey)) {//当eventKey不为空时，说明该用户通过扫码关注的
                Log::info($postObject->EventKey . '扫码关注的用户'.'open_id：'.$postObject->FromUserName);
                $content .= '通过扫码关注';
            }

            //发送模板消息
            $templateController = new Template();
            $data = [
                'desc' => [ 'value'=> '欢迎关注我的公众号', 'color' => '#173177'],
                'test' => [ 'value'=> '测试模板消息推送', 'color' => '#173177'],
                'date' => [ 'value'=> date('Y-m-d H:i:s', $time), 'color' => '#173177'],
            ];
            $templateController->sendTemplate($postObject->FromUserName, '0F-ljRyVMXOTeV0ucGYbFncVkHgbiKm7QG6XKgulhjc', 'www.huanglvshen.cn', [], $data);

            $info = sprintf($template, $toUser, $fromUser, $time, $msgType, $content);
            log_test($info);
            echo $info;

        } else if (strtolower($postObject->Event) == 'scan') {
            //当用户关注之后扫码时
            $eventKey = '';
            //对扫码进来的参数进行判断
            if ($postObject->EventKey == 123) {
                $eventKey = '临时二维码';
            } else if ($postObject->EventKey == "test") {
                $eventKey = '永久二维码';
            }

            $arr = [
                [
                    'title' => '博客',
                    'desc' => '个人博客'.$eventKey,
                    'picurl' => 'https://img4.mukewang.com/5a2f5e580001e80b02140214-100-100.jpg',
                    'url' => 'www.huanglvshen.cn'
                ],
            ];
            $toUser = $postObject->FromUserName;
            $fromUser = $postObject->ToUserName;
            $time = time();
            $total = count($arr);
            $template = "<xml>
                      <ToUserName><![CDATA[{$toUser}]]></ToUserName>
                      <FromUserName><![CDATA[{$fromUser}]]></FromUserName>
                      <CreateTime>{$time}</CreateTime>
                      <MsgType><![CDATA[news]]></MsgType>
                      <ArticleCount>" . $total . "</ArticleCount><Articles>";


            foreach ($arr as $v) {
                $template .= "<item>
                                  <Title><![CDATA[{$v['title']}]]></Title>
                                  <Description><![CDATA[{$v['desc']}]]></Description>
                                  <PicUrl><![CDATA[{$v['picurl']}]]></PicUrl>
                                  <Url><![CDATA[{$v['url']}]]></Url>
                                </item>";
            }

            $template .= "</Articles></xml>";log_test($template);
            $info = $template;
            echo  $info;
        }
    }

    /**
     * 回复文本消息
     * @param object $postObject
     * @return void
     */
    public function reponseContent($postObject)
    {
        //消息模板
        $template = "<xml>
                      <ToUserName><![CDATA[%s]]></ToUserName>
                      <FromUserName><![CDATA[%s]]></FromUserName>
                      <CreateTime>%s</CreateTime>
                      <MsgType><![CDATA[%s]]></MsgType>
                      <Content><![CDATA[%s]]></Content>
                    </xml>";
        //当收到消息为imooc时
        $toUser = $postObject->FromUserName;
        $fromUser = $postObject->ToUserName;
        $time = time();
        $msgType = 'text';
        switch (trim($postObject->Content)) {
            case 'imooc' :
                $content = 'imooc is very good';
                $info = sprintf($template, $toUser, $fromUser, $time, $msgType, $content);
                break;
            case '1' :
                $content = '你输入的是数字1';
                $info = sprintf($template, $toUser, $fromUser, $time, $msgType, $content);
                break;
            case '2' :
                $content = '你输入的是数字2';
                $info = sprintf($template, $toUser, $fromUser, $time, $msgType, $content);
                break;
            case 3 :
                $content = '<a href="www.huanglvshen.cn">个人博客</a>';
                $info = sprintf($template, $toUser, $fromUser, $time, $msgType, $content);
                break;
            case '博客':
                $content = '<a href="www.huanglvshen.cn">个人博客</a>';
                $info = sprintf($template, $toUser, $fromUser, $time, $msgType, $content);
                break;
            case '图文'://回复图文消息
                $arr = [
                    [
                        'title' => 'imooc',
                        'desc' => 'imooc is very good',
                        'picurl' => 'http://mmbiz.qpic.cn/mmbiz/2KJ7ickRkmg0qI1qucRV9MhAeBYBTDjiaEdopV13rYSTsd5co6b2ANK8xczPDIovNcESQgNZDIOW3Hz9wiaCnaGiaA/0?tp=webp&wxfrom=5',
                        'url' => 'www.imooc.com'
                    ],
                    [
                        'title' => '博客',
                        'desc' => '个人博客',
                        'picurl' => 'https://img4.mukewang.com/5a2f5e580001e80b02140214-100-100.jpg',
                        'url' => 'www.huanglvshen.cn'
                    ],
                ];
                $toUser = $postObject->FromUserName;
                $fromUser = $postObject->ToUserName;
                $time = time();
                $total = count($arr);
                $template = "<xml>
                      <ToUserName><![CDATA[{$toUser}]]></ToUserName>
                      <FromUserName><![CDATA[{$fromUser}]]></FromUserName>
                      <CreateTime>{$time}</CreateTime>
                      <MsgType><![CDATA[news]]></MsgType>
                      <ArticleCount>" . $total . "</ArticleCount><Articles>";


                foreach ($arr as $v) {
                    $template .= "<item>
                                  <Title><![CDATA[{$v['title']}]]></Title>
                                  <Description><![CDATA[{$v['desc']}]]></Description>
                                  <PicUrl><![CDATA[{$v['picurl']}]]></PicUrl>
                                  <Url><![CDATA[{$v['url']}]]></Url>
                                </item>";
                }

                $template .= "</Articles></xml>";log_test($template);
                $info = $template;
                break;
            default :
                //发送什么数据，返回什么数据
                $content = $postObject->Content;
                $info = sprintf($template, $toUser, $fromUser, $time, $msgType, $content);
                break;
        }

        //打印数据
        Log::error($info);
        echo $info;

    }

}