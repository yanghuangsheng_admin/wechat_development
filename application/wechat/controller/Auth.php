<?php

namespace app\wechat\controller;

use app\wechat\controller\Index;

/**
 * 操作微信授权
 * Class Auth
 * @package app\wechat\controller
 */
class Auth
{
    /**
     * 构造方法，对参数默认赋值
     * Auth constructor.
     * @param string $scope
     */
    function __construct($scope = 'snsapi_userinfo')
    {
        $this->appid = APPID;
        $this->appsecret = APPSECRET;
        $this->scope = $scope;
    }

    /**
     * 跳转到授权页面
     * @param string $scope
     */
    function getBaseInfo()
    {
        $redirect_uri = urlencode('http://www.huanglvshen.cn/wechat/getUserInfo');//授权后重定向的回调链接地址， 请使用 urlEncode 对链接进行处理
        $scope = $this->scope;//snsapi_base 直接获取用户open_id；snsapi_userinfo 需要授权才能获取用户信息
        $state = 123;//带的参数
        //1.获取code
        $url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid={$this->appid}&redirect_uri={$redirect_uri}&response_type=code&scope={$scope}&state={$state}#wechat_redirect";
        header('Location:'.$url);
    }

    /**
     * 回调地址
     * 获取用户的open_id
     */
    function getUserInfo()
    {
        $input = input();
        $code = $input['code'];
        $state = $input['state'];
        //2.获取到网页授权的access_token
        $url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid={$this->appid}&secret={$this->appsecret}&code={$code}&grant_type=authorization_code";

        //3.拉取用户的open_id
        $result = get($url);
        $result = json_decode($result, true);

        //当scope的为snsapi_userinfo，拉取用户的详情信息
        if ($this->scope == 'snsapi_userinfo') {
            $url = "https://api.weixin.qq.com/sns/userinfo?access_token={$result['access_token']}&openid={$result['openid']}&lang=zh_CN";
            $data = get($url);
            $data = json_decode($data);
            var_dump($data);
        }
    }



}