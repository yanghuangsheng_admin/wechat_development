<?php
namespace app\wechat\controller;

use app\wechat\service\Wechat;
use think\Controller;
use app\wechat\common\ErrorCode;
use app\wechat\common\PKCS7Encoder;
use app\wechat\common\Prpcrypt;
use app\wechat\common\SHA1;
use app\wechat\common\XMLParse;
use think\Log;

/**
 * Class Index
 * @package app\wechat\controller
 */
class Index extends Controller
{

    use \app\index\controller\TraitTest;//引入teait特性

    public $token = TOKEN;
    public $appId = APPID;
    public $appSecret = APPSECRET;
    public $encodingAesKey = ENCODINGAESKEY;


    /**
     * @param object $request
     * @param array $get
     * return void
     */
    public function index()
    {
        $echostr = input('echostr', '');
        $nonce = input('nonce', '');
        $timestamp = input('timestamp', '');
        $signature = input("signature", '');
        $msg_signature = input("msg_signature", '');
        //因为当接入成功后(也就是点击提交，接入成功后 )，微信服务器不再发送echostr这个值
        if(!empty($echostr)) {
            log_test('验证成功');
            //接入成功后一定要echo $echostr
            $this->checkSignature($nonce, $timestamp, $signature, $echostr);
            exit();
        } else {
            //处理消息和事件
            $wechat = new Wechat();
            // 接收到微信以POST发送过来的数据
            $xmlStr = file_get_contents("php://input");

            //$xmlStr = $GLOBALS['HTTP_RAW_POST_DATA'];//获取微信的xml数据
            //安全模式下需要解密数据
//            $postObject = "";
//            $code = $this->decryptMsg($msg_signature, $timestamp, $nonce, $xmlStr, $postObject);
//            if ($code != 0) {
//                Log::error($code);
//                return $code;
//            }
            $postObject = simplexml_load_string($xmlStr, 'SimpleXMLElement', LIBXML_NOCDATA);

            log_test($postObject);
            //接收消息推送
            if ( strtolower($postObject->MsgType) == 'event') {
                $wechat->reponseEvent($postObject);
            }

            //接收文本消息
            if ( strtolower($postObject->MsgType) == 'text') {
                $wechat->reponseContent($postObject);
            }

        }


    }

    /**
     * 获取access_token
     * @return mixed
     * @throws \Exception
     */
    public function getAccessToken()
    {
        $url = "https://api.weixin.qq.com/cgi-bin/token";
        $params = [
            'grant_type' => 'client_credential',
            'appid' => $this->appId,
            'secret' => $this->appSecret,
        ];
        $redis = new \Redis();
        $redis->connect(REDIS_HOST);
        //判断redis中是否存在access_token
        if (!$redis->exists('access_token')) {
            $result = http($url, $params);
            log_test($result);

            $result = json_decode($result, true);
            if (isset($result['errcode'])) {
                log_test('获取access_token失败');
                exit();
            }

            $redis->setex('access_token', $result['expires_in'] - 100, $result['access_token']);
            $access_token =  $result['access_token'];
        } else {
            $access_token = $redis->get('access_token');
        }
        log_test($access_token);
        return $access_token;
    }



    /**
     * 验证微信公众号接入
     *
     * @param string $nonce
     * @param string $timestamp
     * @param string $signature
     * @param string $echostr  --接入成功后需要输出$echostr
     * @return void
     * @Description
     * @example
     * @author hls
     * @since
     * @Date
     * @Email 1160957956@qq.com
     */
    public function checkSignature($nonce, $timestamp, $signature, $echostr)
    {
        $tmpArr = array($this->token, $timestamp, $nonce);
        sort($tmpArr, SORT_STRING);
        $tmpStr = implode( $tmpArr );
        $tmpStr = sha1( $tmpStr );
        //log_test($tmpStr);log_test($signature);
        if ($tmpStr == $signature) {
            echo $echostr;
            log_test('接入成功'.$echostr);
        } else {
            return false;
        }
    }


    /**
     * 将公众平台回复用户的消息加密打包.
     * <ol>
     *    <li>对要发送的消息进行AES-CBC加密</li>
     *    <li>生成安全签名</li>
     *    <li>将消息密文和安全签名打包成xml格式</li>
     * </ol>
     *
     * @param $replyMsg string 公众平台待回复用户的消息，xml格式的字符串
     * @param $timeStamp string 时间戳，可以自己生成，也可以用URL参数的timestamp
     * @param $nonce string 随机串，可以自己生成，也可以用URL参数的nonce
     * @param &$encryptMsg string 加密后的可以直接回复用户的密文，包括msg_signature, timestamp, nonce, encrypt的xml格式的字符串,
     * 当return返回0时有效
     *
     * @return int 成功0，失败返回对应的错误码
     */
    public function encryptMsg($replyMsg, $timeStamp, $nonce, &$encryptMsg)
    {
        $pc = new Prpcrypt($this->encodingAesKey);

        //加密
        $array = $pc->encrypt($replyMsg, $this->appId);
        $ret = $array[0];
        if ($ret != 0) {
            return $ret;
        }

        if ($timeStamp == null) {
            $timeStamp = time();
        }
        $encrypt = $array[1];

        //生成安全签名
        $sha1 = new SHA1;
        $array = $sha1->getSHA1($this->token, $timeStamp, $nonce, $encrypt);
        $ret = $array[0];
        if ($ret != 0) {
            return $ret;
        }
        $signature = $array[1];

        //生成发送的xml
        $xmlparse = new XMLParse;
        $encryptMsg = $xmlparse->generate($encrypt, $signature, $timeStamp, $nonce);
        return ErrorCode::$OK;
    }


    /**
     * 检验消息的真实性，并且获取解密后的明文.
     * <ol>
     *    <li>利用收到的密文生成安全签名，进行签名验证</li>
     *    <li>若验证通过，则提取xml中的加密消息</li>
     *    <li>对消息进行解密</li>
     * </ol>
     *
     * @param $msgSignature string 签名串，对应URL参数的msg_signature
     * @param $timestamp string 时间戳 对应URL参数的timestamp
     * @param $nonce string 随机串，对应URL参数的nonce
     * @param $postData string 密文，对应POST请求的数据
     * @param &$msg string 解密后的原文，当return返回0时有效
     *
     * @return int 成功0，失败返回对应的错误码
     */
    public function decryptMsg($msgSignature, $timestamp = null, $nonce, $postData, &$msg)
    {
        if (strlen($this->encodingAesKey) != 43) {
            return ErrorCode::$IllegalAesKey;
        }

        $pc = new Prpcrypt($this->encodingAesKey);

        //提取密文
        $xmlparse = new XMLParse;
        $array = $xmlparse->extract($postData);

        $ret = $array[0];

        if ($ret != 0) {
            return $ret;
        }

        if ($timestamp == null) {
            $timestamp = time();
        }

        $encrypt = $array[1];
        $touser_name = $array[2];

        //验证安全签名
        $sha1 = new SHA1;
        $array = $sha1->getSHA1($this->token, $timestamp, $nonce, $encrypt);

        $ret = $array[0];

        if ($ret != 0) {
            return $ret;
        }

        $signature = $array[1];

        if ($signature != $msgSignature) {
            return ErrorCode::$ValidateSignatureError;
        }

        $result = $pc->decrypt($encrypt, $this->appId);
        if ($result[0] != 0) {
            return $result[0];
        }
        $msg = simplexml_load_string($result[1], 'SimpleXMLElement', LIBXML_NOCDATA);

        return ErrorCode::$OK;
    }




}