<?php

namespace app\wechat\controller;

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/5/7
 * Time: 20:22
 */

/**
 * 菜单管理--订阅号没有权限使用。。
 * Class Menu
 */
class Menu
{
    /**
     * 增加自定义菜单
     * @return string|\think\response\Json
     * @throws \Exception
     */
    public function addButton()
    {
        $url = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=" . getAccessToken();
        echo getAccessToken();
        //如果使用数据传值，中文字符串要使用urlencode();
        //在转码为json格式后，urldecode(json_encode($arr))
        $params = '{
                     "button":[
                          {
                               "name":"扫码",//urlencode(扫码)
                               "sub_button":[
                               {
                                   "type":"view",
                                   "name":"博客",
                                   "url":"http://www.huanglvshen.cn"
                               },
                               {
                                    "type": "scancode_waitmsg",
                                    "name": "扫码带提示",
                                    "key": "rselfmenu_0_0",
                                    "sub_button": [ ]
                               },
                               {
                                    "type": "scancode_push",
                                    "name": "扫码推事件",
                                    "key": "rselfmenu_0_1",
                                    "sub_button": [ ]
                               }]
                          },
                          {
                               "name": "发图",
                               "sub_button": [
                                    {
                                        "type": "pic_sysphoto",
                                        "name": "系统拍照发图",
                                        "key": "rselfmenu_1_0",
                                        "sub_button": []
                                    },
                                    {
                                        "type": "pic_photo_or_album",
                                        "name": "拍照或者相册发图",
                                        "key": "rselfmenu_1_1",
                                        "sub_button": []
                                    },
                                    {
                                        "type": "pic_weixin",
                                        "name": "微信相册发图",
                                        "key": "rselfmenu_1_2",
                                        "sub_button": []
                                   }]
                          },
                          {
                               "name": "发送位置",
                               "type": "location_select",
                               "key": "rselfmenu_2_0"
                          }]
                    }';
        $result = post($url, $params);
        $result = json_decode($result, true);
        halt($result);
        if ($result['errcode'] != 0) {
            return json(['code' => $result['errcode'], 'msg' => $result['errmsg']]);
        }
        return '增加菜单成功';
    }

    /**
     * 查询自定义菜单
     * @return string|\think\response\Json
     * @throws \Exception
     */
    public function readButton()
    {
        $url = "https://api.weixin.qq.com/cgi-bin/menu/get?access_token=" . getAccessToken();
        $result = get($url);halt($result);
        $result = json_decode($result, true);
        return $result;
    }

    /**
     * 删除自定义菜单
     * @return string|\think\response\Json
     * @throws \Exception
     */
    public function deleteButton()
    {
        $url = "https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=" . getAccessToken();
        $result = get($url);halt($result);
        $result = json_decode($result, true);
        return $result;
    }
}
