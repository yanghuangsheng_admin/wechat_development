<?php

namespace app\wechat\controller;

use app\wechat\controller\Index;


/**
 * 获取二维码
 * Class Code
 * @package app\wechat\controller
 */
class Code
{
    private $access_token = '';
    function __construct()
    {
        $index = new Index();
        $this->access_token = $index->getAccessToken();
    }

    /**
     * 获取二维码
     * @param $ticket 生成的票据
     */
    public function getCode()
    {
        header('Content-type:text/html;charset=utf-8');
        $url = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=" . $this->access_token;
        $data = '{
            "expire_seconds": 604800,
            "action_name": "QR_LIMIT_SCENE", 
            "action_info": 
            {"scene": 
                {"scene_id": 123}
            }
        }';
        $result = post($url, $data);
        $result = json_decode($result, true);

        $ticket = urlencode($result['ticket']);
        $url = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=" . $ticket;
        echo '临时二维码';
        echo '<img src="' .$url. '">';
    }

    /**
     * 生成票据
     * @return $result
     */
    public function getTicket()
    {
        $url = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=" . $this->access_token;
        $data = '{
            "expire_seconds": 604800,
            "action_name": "QR_LIMIT_SCENE", 
            "action_info": 
            {"scene": 
                {"scene_id": 123}
            }
        }';
        $result = post($url, $data);
        $result = json_decode($result, true);
        var_dump($result);
        return $result;
    }

    //获取永久二维码
    public function getPerpetualCode()
    {
        $url = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=" . $this->access_token;
        $data = '{
            "action_name": "QR_LIMIT_STR_SCENE", 
            "action_info": 
            {"scene": 
                {"scene_str": "test"}
            }
        }';
        $result = post($url, $data);
        $result = json_decode($result, true);
        $ticket = urlencode($result['ticket']);
        $url = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=" . $ticket;
        echo '永久二维码';
        echo '<img src="' .$url. '">';
    }
}