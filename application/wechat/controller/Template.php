<?php
namespace app\wechat\controller;

use app\wechat\controller\Index;
/**
 * 操作模板消息
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/5/13
 * Time: 17:58
 */
class Template
{
    private $access_token;
    function __construct()
    {
        $index = new Index();
        $this->access_token = $index->getAccessToken();
    }

    /**
     * 发送模板消息
     * @param $toUser 用户的open_id
     * @param $template_id 模板id
     * @param string $skipUrl 跳转的url
     * @param array $miniprogram 小程序数据
         * [ "appid":"xiaochengxuappid12345",小程序的appid
            "pagepath":"index?foo=bar" 小程序跳转路径
         * ]
     * @param $data 发送数据
     * @return json
     */
    public function sendTemplate($toUser, $template_id, $skipUrl = 'www.huanglvshen.cn', $miniprogram = [], $data)
    {
        $url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=".$this->access_token;

        $toUser = json_encode($toUser);
        $arr = json_decode($toUser, true);
        $toUser = $arr[0];log_test($arr);

        $param = [
            'touser' => $toUser,
            'template_id' => $template_id,
            'url' => $skipUrl,
            'miniprogram' => $miniprogram,
            'data' => $data
        ];
        log_test($param);
        $param = json_encode($param);
        $res = post($url, $param);
        log_test($res);

    }

    /**
     * 测试模板消息发送
     */
    public function testSendTemplate($skipUrl = 'www.huanglvshen.cn')
    {
        $data = [
            'desc' => [ 'value'=> '欢迎关注我的公众号', 'color' => '#173177'],
            'test' => [ 'value'=> '测试模板消息推送', 'color' => '#173177'],
            'date' => [ 'value'=> date('Y-m-d H:i:s', time()), 'color' => '#173177'],
        ];
        $url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=".$this->access_token;
        $param = [
            'touser' => 'o9m5V1lFRgogB7J4qQSOd6UN2HIo',
            'template_id' => '0F-ljRyVMXOTeV0ucGYbFncVkHgbiKm7QG6XKgulhjc',
            'url' => $skipUrl,
            'miniprogram' => [],
            'data' => $data
        ];
        $param = json_encode($param);
        $res = post($url, $param);
        var_dump($res);

    }
}