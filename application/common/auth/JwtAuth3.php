<?php
namespace app\common\auth;

class JwtAuth3 {
    /**
     * 单例模式
     *
     * @var [type]
     * @Description
     * @example
     * @author hls
     * @since
     * @Date
     * @Email 1160957956@qq.com
     */
    private static $instance;
    
    /**
     * 返回实例
     *
     * @return void
     * @Description
     * @author hls
     * @since
     * @Email 1160957956@qq.com
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * 私有化构造函数
     *
     * @Description
     * @author hls
     * @since
     * @Email 1160957956@qq.com
     */
    private function __construct()
    {
        // 
    }

    /**
     * 私有化clone函数
     *
     * @return void
     * @Description
     * @author hls
     * @Date
     * @Email 1160957956@qq.com
     */
    private function __clone()
    {
        //
    }
}