<?php
namespace app\common\auth;

use \Firebase\JWT\JWT;
use think\Exception;

/**
 * 使用jwt2.0的版本
 *
 * @Description
 * @author hls
 * @since
 * @Email 1160957956@qq.com
 */
class JwtAuth2 {
    /**
     * 单例模式
     *
     * @var [type]
     * @Description
     * @example
     * @author hls
     * @since
     * @Date
     * @Email 1160957956@qq.com
     */
    private static $instance;
    private $user_id;
    
    /**
     * 返回实例
     *
     * @return void
     * @Description
     * @author hls
     * @since
     * @Email 1160957956@qq.com
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * 设置用户id
     *
     * @param [type] $user_id
     * @return $this
     * @Description
     * @author hls
     * @since
     * @Email 1160957956@qq.com
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
        return $this;
    }

    /**
     * 加密信息
     *
     * @return void
     * @Description
     * @author hls
     * @since
     * @Email 1160957956@qq.com
     */
    function encode()
    {
        $key = 'key';//加密的key
        $payload = [
            'iss' => '127.0.0.1',//签发者
            'sub' => 'user',//主题
            'iat' => time(),//签发时间
            'exp' => time() + 3600,//过期时间
            'uid' => $this->user_id,//用户id
        ];//payload
        $alg = 'HS256';//使用的算法
        $token = JWT::encode($payload, $key, $alg);
        return (string)$token;
    }

    /**
     * 解码并验证token
     *
     * @param [type] $token
     * @param string $key
     * @param array $allowed_algs
     * @return void
     * @Description
     * @author hls
     * @since
     * @Email 1160957956@qq.com
     */
    function decodeToken($token, $key = 'key', $allowed_algs = ['HS256'])
    {
        try {
            $payload = JWT::decode($token, $key, $allowed_algs);
            if ( ($payload->iss == '127.0.0.1') && ($payload->sub == 'user') && ($payload->uid == 1) ) {
                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            return [
                'code' => 999,
                'msg' => '参数错误',
                'data' => $e->getMessage(),
            ];
        }        
    }

    /**
     * 私有化构造函数
     *
     * @Description
     * @author hls
     * @since
     * @Email 1160957956@qq.com
     */
    private function __construct()
    {
        // 
    }

    /**
     * 私有化clone函数
     *
     * @return void
     * @Description
     * @author hls
     * @Date
     * @Email 1160957956@qq.com
     */
    private function __clone()
    {
        //
    }
}