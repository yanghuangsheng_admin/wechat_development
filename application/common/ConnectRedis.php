<?php 
namespace app\common;

class ConnectRedis
{
	private $redis;

	public function __construct()
	{
		$this->redis = new \Redis();
		$this->redis->connect(REDIS_HOST, 6379);
	}

	/**
	 * [getClient description] 链接原生redis,获取redis实例在调用的时候返回
	 * @param  integer $db [description] 
	 * @return [type]      [description]
	 */
	
	 /*使用如下,可以使用原生redis的全部方法
	 $redis = new ConnectRedis();
         $redis = $redis->getClient(1);
     	 */
	 
	public function getClient($db = 1)
	{
		$this->redis->select($db);
		return $this->redis;
	}
}
