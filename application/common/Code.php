<?php

namespace app\common;

/**
 * 定义统一的返回信息
 *
 * @Description
 * @author hls
 * @since
 * @Email 1160957956@qq.com
 */
class Code
{
    const SUCCESS = [0, 'success'];


    const ERR_PASSWORD = [1001, '密码错误'];
}