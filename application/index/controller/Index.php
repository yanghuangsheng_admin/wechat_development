<?php
namespace app\index\controller;

use think\Exception;
use \think\Log;
use think\Validate;
use app\index\service\ProductService;

class Index extends ResponseJson
{

    public function index()
    {
        return "<h1>欢迎来到测试界面</h1>";
    }

    public function buy()
    {
        $input = input();
        $input['id'] = isset($input['id']) ? $input['id'] : 1;
        $validate = new Validate([
            'id' => 'require|number',
        ]);

        if (!$validate->check($input)) {
            return $this->errorJson($validate->getError());
        }

        try {
            $result = ProductService::getStock($input);
            if (isset($result['code'])) {
                return $this->errorJson($result['message'], $result['code']);
            }
            return $this->json();
        } catch (Exception $e) {
            return $this->errorJson($e->getMessage());
        }
    }

}
