<?php

namespace app\index\controller;

use think\Controller;
use app\common\auth\JwtAuth2;
USE app\common\auth\JwtAuth3;
use app\common\Code;
class User extends Controller
{
    public function login()
    {
        $user_id = 1;
        $jwt2 = JwtAuth2::getInstance();
        $token = $jwt2->setUserId(1)->encode();
        $result = [
            'code' => Code::SUCCESS[0],
            'msg' => Code::SUCCESS[1],
            'data' => $token,
        ];
        return json_encode($result);
    }
}