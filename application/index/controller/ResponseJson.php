<?php
namespace app\index\controller;

use think\Controller;
use app\common\auth\JwtAuth2;
use app\common\auth\JwtAuth3;
use think\Validate;
use think\exception\HttpException;
use think\exception\HttpResponseException;
use think\Response;

/**
 * 定义初始化信息返回
 *
 * @Description
 * @example
 * @author hls
 * @since
 * @Date
 * @Email 1160957956@qq.com
 */
class ResponseJson extends Controller  {

    public function __construct()
    {
        $rule = [
            ['token','require','token必须'],
        ];
        
        $data = input();
        $validate = new Validate($rule);
        $result   = $validate->check($data);
        if(!$result){
            //halt($validate->getError());
            //throw new HttpResponseException(Response::create($this->error(999), 'json', 200, []));
        }
        
        $token = input('token');
        $jwt2 = JwtAuth2::getInstance();
        $jwt3 = JwtAuth2::getInstance();
        //解码token并验证
        $payload = $jwt2->decodeToken($token);//halt($payload);
        if (isset($payload['code'])) {
            //halt('参数错误');
        }
    }
    /**
     * 接口返回正确的json数据信息
     *
     * @param array $data
     * @return void
     * @author hls
     * @since
     * @Date
     * @Email 1160957956@qq.com
     */
    public function json($data = [])
    {
        return $this->responseJson(0, 'success', $data);
    }

    /**
     * 接口返回错误的json数据信息
     *
     * @param [type] $code
     * @param string $msg
     * @param array $data
     * @return void
     * @author hls
     * @since
     * @Date
     * @Email 1160957956@qq.com
     */
    public function errorJson($msg = '', $code = 999, $data = [])
    {
        return $this->responseJson($code, $msg, $data);
    }

    /**
     * 返回json数据
     *
     * @param [type] $code
     * @param [type] $msg
     * @param [type] $data
     * @return void
     * @author hls
     * @since
     * @Date
     * @Email 1160957956@qq.com
     */
    private function responseJson($code, $msg, $data)
    {   
        $result = [
            'code' => $code,
            'msg' => $msg,
            'data' => $data,
        ];
        return json_encode($result);
    }    
}