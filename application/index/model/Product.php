<?php

namespace app\index\model;

use think\Model;

class Product extends Model
{
    protected $table = 'product';

    //设置返回数据集的对象名
    protected $resultSetType = 'collection';
}