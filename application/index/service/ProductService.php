<?php

namespace app\index\service;

use think\Exception;
use app\index\model\Product as ProductModel;

/**
 * 秒杀测试
 * ab -n 请求书 -c 并发数 url地址
 * ab -c 500 -n 5000 http://127.0.0.1/hls/public/index.php/buy
 * Class ProductService
 * @package app\index\service
 */
class ProductService
{
    /**
     * 对并发进行处理
     * @param $where
     * @return array
     * @throws Exception
     */
    public static function getStock($where)
    {
        $id = isset($where['id']) ? intval($where['id']) : 1;
        $redis = new \Redis();
        $redis->connect(REDIS_HOST);
        try {
            $productModel = new ProductModel();
            //获取库存
            if ($redis->exists('stock'.$id) && $redis->get('stock'.$id) > 0) {
                $stock = $redis->get('stock'.$id);
            } else {
                $stock = $productModel->where('id', $id)->value('stock');
                $redis->set('stock'.$id, $stock);
            }

            //日志记录

            //监视key值，当这个key被改变后，之后的事务会被打断
            $redis->watch('sales'.$id);
            $sales = $redis->get('sales'.$id);
            if ($stock <= $sales) {
                //减少库存
                $data = $productModel->where('id', $id)->setDec('stock', $sales);
                return ['code' => 999, 'message' => '活动已结束'];
            }

            //保证一组命令要么都执行，要么都不执行
            $redis->multi();//开始事务
            //$redis->decr('stock'.$id);//减少库存
            $redis->incr('sales'.$id);//增加销量
            //sleep(1);
            $redis->exec();

            //生成订单
            //支付
        } catch (Exception $e) {
            throw $e;
        }
    }
}